# -*- coding: utf-8 -*-

'''
Created on 2013/02/27

@author: user
'''

import logging
logging.basicConfig(
    level=logging.INFO,
    format="%(levelname)-8s %(module)-16s %(funcName)-16s@%(lineno)d - %(message)s"
)

import unittest
from unpack import read_int
from lucene import read_vint, read_utf8, UTF8String


class FDTEntry(object):
    
    def __init__(self, fnm):
        self._fnm = fnm
    
    def set_field(self, n, value):
        name = self._fnm[n-1]
        if name == u"Speech":
            self.speech = value
        elif name == u"Index":
            self.index = value
        elif name == u"Example":
            self.example = value
        elif name == u"Key":
            self.key = value
        elif name == u"Id":
            self.id = value
        elif name == u"SubId":
            self.sub_id = value
        elif name == u"WordForm1":
            self.word_form1 = value
        elif name == u"WordForm2":
            self.word_form2 = value
        elif name == u"WordForm3":
            self.word_form3 = value
        elif name == u"WordForm4":
            self.word_form4 = value
        elif name == u"Comment":
            self.comment = value
        else:
            raise Exception("Error: encountered unknown field: %s" % name)
            
    def __str__(self):
        return (("Id=%s, SubId=%s, Speech=%s, Key=%s, " + 
                 "Index=%s, Comment=%s, Example=%s, " +
                 "WordForm1=%s, WordForm2=%s, WordForm3=%s, WordForm4=%s") % (
                 self.id, self.sub_id, self.speech, self.key, 
                 self.index, self.comment, self.example,
                 self.word_form1, self.word_form2, self.word_form3, self.word_form4))
        

class FDTParser(object):
            
    def _set_pos(self, n):
        self._pos = n
        logging.debug("position moved to %s", hex(self._pos))
        
    def _add_pos(self, n):
        self._pos += n
        logging.debug("position moved to %s", hex(self._pos))
    
    def _read_vstr(self, buf):
        vint = read_vint(buf, self._pos)
        logging.debug(vint)
        self._add_pos(vint.length)
        
        if vint.value == 0:
            return UTF8String(0, "") 
        
        vstr = read_utf8(buf, self._pos, vint.value)
        logging.debug(vstr)
        self._add_pos(vstr.length)
        return vstr
    
    def accept(self, buf, fnm, visitor):
        eof = len(buf)
        logging.debug("parsing FDT of %s(%s) bytes", eof, hex(eof))
        self._set_pos(0)

        while True:
            if eof == self._pos:
                break
            
            length = read_vint(buf, self._pos)
            logging.debug("field length: %s", length.value)
            if length.value == 0:
                raise Exception()
            self._add_pos(length.length)
                    
            entry = FDTEntry(fnm)
            for i in xrange(length.value):
                field_id = read_vint(buf, self._pos)
                logging.debug("field id: %s", field_id.value)
                self._add_pos(field_id.length)
                
                logging.debug("skip a byte of (%s)", hex(read_int(buf, self._pos, 1)))
                self._add_pos(1)
                
                value = self._read_vstr(buf)
                entry.set_field(field_id.value, value.value)
                
            visitor.visit(entry)
        logging.debug("parse finishied")


class TestFDTParser(unittest.TestCase):
    def test_parse(self):
        import os
        from edic2 import EDIC
        from fnm import FNMParser 
        edic = EDIC("dicts/config.ini")
        for dic in edic.dictionaries():
            name = os.path.splitext(os.path.split(dic.path)[1])[0]
            path = "test/inx/" + dic.info.tablename + "/" + name 
            visitor = FDTParserVisitorExample()
            fnm = FNMParser().parse(open(path + ".fnm", "rb").read())
            FDTParser().accept(open(path + ".fdt", "rb").read(), fnm, visitor)
            
        
class FDTParserFragment(object):
    pass


class FDTParserExample(FDTParserFragment):
    def __init__(self, main_id, sub_id, heading):
        self.main_id = main_id
        self.sub_id = sub_id
        self.heading = heading

    def __str__(self):
        return "%s" % self.heading


class FDTParserDocument(FDTParserFragment):
    def __init__(self, main_id, sub_id, heading, body):
        self.main_id = main_id
        self.sub_id = sub_id
        self.heading = heading
        self.body = body

    def __str__(self):
        return u"%s %s" % (self.heading, self.body)
    

class FDTParserVisitor(object):
    def visit(self, entry):
        raise NotImplementedError()


class FDTParserVisitorExample(FDTParserVisitor):
    def __init__(self):
        self.count = 0
        
    def visit(self, entry):
        logging.info("entry %s: %s", self.count, entry)
        self.count += 1
        
