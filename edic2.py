# -*- coding: utf-8 -*-

'''
Created on 2013/03/06

@author: user
'''

import logging
logging.basicConfig(
    level=logging.DEBUG,
    format="%(levelname)-8s %(module)-16s %(funcName)-16s@%(lineno)d - %(message)s"
)

import unittest
import os
from xml.etree import ElementTree
from inx import INXParser
from fdt import FDTParser, FDTParserVisitorExample
from fnm import FNMParser


class DBInfo(object):
    def __init__(self, filename, tablename, tableformat, dicname, dicshortname):
        self.filename = filename
        self.tablename = tablename
        self.tableformat = tableformat
        self.dicname = dicname
        self.dicshortname = dicshortname
        
    def __str__(self):
        return "fileame=%s, tablename=%s, tableformat=%s, dicname=%s, dicshortname=%s" % (
               self.filename, self.tablename, self.tableformat, self.dicname, self.dicshortname) 


class DIC(object):
    def __init__(self, path, info):
        self.path = path
        self.info = info
        logging.debug("path: %s", self.path)
        logging.debug("info: %s", self.info)
        
    def accept(self, visitor):
        result = INXParser().parse(self.path)
        fdt = None
        fnm = None
        for entry in result.entries:
            if entry.name.endswith(".fdt"):
                fdt = entry
            elif entry.name.endswith(".fnm"):
                fnm = entry
            if fdt and fnm:
                break
        parser = FDTParser()
        parser.accept(fdt.get_content(), FNMParser().parse(fnm.get_content()), visitor)
    
    
    def __str__(self):
        return "path=%s, info=%s" % (self.path, self.info)
    

class EDIC(object):
    def __init__(self, path):
        logging.debug("path: %s", path)
        self.path = path
        
    def dictionaries(self):
        arr = []
        parent = os.path.split(os.path.abspath(self.path))[0] 
        tree = ElementTree.parse(open(self.path, "r"))
        for item in tree.getiterator("DBInfo"):
            info = DBInfo(unicode(item.get("FileName")), 
                          unicode(item.get("TableName")), 
                          unicode(item.get("TableFormat")), 
                          unicode(item.get("DicName")), 
                          unicode(item.get("DicShortName")))
            db_dir = parent + "/" + info.tablename
            inx_file = self._get_inx_file(db_dir)
            if inx_file:
                dic = DIC(inx_file, info)
                arr.append(dic)
            else:
                logging.debug("skipped; inx file not found")    
        return arr
    
    def _get_inx_file(self, path):
        for f in os.listdir(path):
            if f.endswith(".inx"):
                return path + "/" + f
        
        
class TestCatalog(unittest.TestCase):
    def test_parse_header(self):
        edic = EDIC("dicts/config.ini")
        for dic in edic.dictionaries():
            logging.debug("dic: %s", dic)
            dic.accept(FDTParserVisitorExample(dic.info))
            
            