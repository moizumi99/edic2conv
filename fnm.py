# -*- coding: utf-8 -*-

'''
Created on 2013/03/07

@author: user
'''

import logging
logging.basicConfig(
    level=logging.DEBUG,
    format="%(levelname)-8s %(module)-16s %(funcName)-16s@%(lineno)d - %(message)s"
)

import unittest
from unpack import find
from lucene import read_vint, read_utf8


class FNMParser(object):
        
    def _set_pos(self, n):
        self._pos = n
        logging.debug("position moved to %s", hex(self._pos))
        
    def _add_pos(self, n):
        self._pos += n
        logging.debug("position moved to %s", hex(self._pos))
    
    def _read_vstr(self, buf):
        vint = read_vint(buf, self._pos)
        logging.debug(vint)
        self._add_pos(vint.length)
        
        vstr = read_utf8(buf, self._pos, vint.value)
        logging.debug(vstr)
        self._add_pos(vstr.length)
        return vstr
    
    def parse(self, buf):
        logging.debug("parsing FNM of %s(%s) bytes", len(buf), hex(len(buf)))
        self._set_pos(0)
        arr = []
        length = read_vint(buf, self._pos)
        logging.debug("field length: %s", length.value)
        
        if length.value == 0:
            raise Exception()
        self._add_pos(length.length)
        
        if self._pos == find(buf, self._pos, 0x00):
            logging.debug("skip a empty entry of 0x00")
            self._add_pos(1)
        else:
            logging.error("byte of this position must be 0x00")
            raise Exception()
        
        for i in xrange(length.value - 1):
            logging.debug("skip a byte of 0x01 or 0x00")
            self._add_pos(1)
            
            field = self._read_vstr(buf)
            logging.debug("field %s is %s", i+1, field)
            arr.append(field.value)
                
        logging.debug("parse finishied")
        return arr

class TestFNMParser(unittest.TestCase):
    def test_parse(self):
        import os
        from edic2 import EDIC
        edic = EDIC("dicts/config.ini")
        for dic in edic.dictionaries():
            name = os.path.splitext(os.path.split(dic.path)[1])[0] + ".fnm"
            path = "test/inx/" + dic.info.tablename + "/" + name 
            FNMParser().parse(open(path, "rb").read())
        
        