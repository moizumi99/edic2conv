# -*- coding: utf-8 -*-

'''
Created on 2013/03/07

@author: user
'''

import logging
logging.basicConfig(
    level=logging.DEBUG,
    format="%(levelname)-8s %(module)-16s %(funcName)-16s@%(lineno)d - %(message)s"
)

from unpack import read_int, read_str

def read_utf8(f, p, n):
    length = 0
    eof = len(f)
    buf = ""
    while True:
        if p == eof:
            raise EOFError()
        buf += read_str(f, p, 1)
        p += 1
        length += 1
        if n <= length:
            try:
                text = unicode(buf, "utf-8")
                if len(text) == n:
                    return UTF8String(length, text) 
            except UnicodeDecodeError:
                pass

def read_vint(f, p):
    """
    Decodes variable-length format for position integers.
    http://lucene.apache.org/core/old_versioned_docs/versions/3_0_0/fileformats.html#VInt
    """
    eof = len(f)
    length = 0
    total = 0
    while True:
        if p == eof:
            raise EOFError()
        i = read_int(f, p, 1)
        has_remain = False
        if 0b10000000 <= i: #high-order bit is 1
            has_remain = True
            i = i ^ 0b10000000 #high-order bit to 0
        else:
            has_remain = False
        total = total | i << 7 * length #appends low-order 7 bits to the result
        length += 1
        p += 1
        if not has_remain:
            break
    return VInt(length, total)


class LuceneFDTDataType(object):
    pass


class VInt(LuceneFDTDataType):
    def __init__(self, length, value):
        self.length = length
        self.value = value
        
    def __str__(self):
        return "VInt: length=%s, value=%s" % (self.length, self.value)
    
    def __int__(self):
        return self.value
        
    
class UTF8String(LuceneFDTDataType): 
    def __init__(self, length, value):
        self.length = length
        self.value = value
               
    def __str__(self):
        return "UTF8String: length=%s, value=%s" % (self.length, self.value)
    
    def __int__(self):
        return self.value



